public class FabricaFiguras {
    
    public static Cuadrado crear_cuadrado(double lado){
        return new Cuadrado(lado);
    }

    public static Circulo crear_circulo(double radio){
        return new Circulo(radio);
    }

}
