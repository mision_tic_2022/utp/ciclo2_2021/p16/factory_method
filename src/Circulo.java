public class Circulo extends Figura {
    //Atributos
    private double radio;

    //Constructor
    public Circulo(double radio){
        this.radio = radio;
    }

    //Getters and Setters
    public double getRadio(){
        return this.radio;
    }

    public void setRadio(double radio){
        this.radio = radio;
    }

    //Acciones
    public double getPerimetro(){
        return 2 * Math.PI * this.radio;
    }

    public double getArea(){
        //return Math.PI * this.radio * this.radio;
        return Math.PI * Math.pow(this.radio, 2);
    }
    
}
