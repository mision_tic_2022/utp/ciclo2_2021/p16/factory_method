public class Cuadrado extends Figura {
    //Atributos
    private double lado;

    //Constuctor
    public Cuadrado(double lado){
        this.lado = lado;
    }

    //Getters and Setters
    public double getLado(){
        return this.lado;
    }

    public void setLado(double lado){
        this.lado = lado;
    }

    //Acciones
    public double getPerimetro(){
        return 4 * lado;
    }

    public double getArea(){
        return 2 * this.lado;
    }
}
